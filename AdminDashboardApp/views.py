# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
def index(request):
	return render(request,'AdminDashboardApp/index.html')

def index1(request):
	return render(request,'AdminDashboardApp/forms.html')

def index2(request):
	return render(request,'AdminDashboardApp/charts.html')

def index3(request):
	return render(request,'AdminDashboardApp/tables.html')

def index4(request):
	return render(request,'AdminDashboardApp/login.html')

def index5(request):
	return render(request,'AdminDashboardApp/register.html')

# Create your views here.
