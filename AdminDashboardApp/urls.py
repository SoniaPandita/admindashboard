from django.conf.urls import url,include
from . import views
app_name = 'admindashboard'
urlpatterns = [
    url(r'^forms/$', views.index1,name='forms'),
     url(r'^charts/$', views.index2,name='charts'),
     url(r'^tables/$', views.index3,name='tables'),
     url(r'^login/$', views.index4,name='login'),
     url(r'^register/$', views.index5,name='register'),
]
